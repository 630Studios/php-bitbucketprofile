<?php
/***********************************************
 * PHP Bitbucket Profile
 **********************************************
 * Description: Generates a clean mini bitbucketProfile for use in a php website.
 * It pulls a First name, Last name, Avatar Image, and Repository List from bitbucket
 * based on the supplied account name.
 *
 * Created By: Jason Penick
 * Created On: 2/20/2016
 * Email: StrikeBackStudios@gmail.com
 * 
 * LICENSE: APDGUT V1 (Anti Profit, Demonology, Gnome and Unicorn Trafficing License)
 * You can do what you want with this except the follow:
 *		1) Claim you wrote it and sell it for one billion dollars
 *      2) Play it backwards on a record player while chanting naked in the forest.
 *      3) Trade this source code with a gnome for a unicorn in return.
 *
 *
 * FUNCTIONS:
 *
 * Name: generateBitBucketProfile($title, $accountName, $email)
 * Description: Pulls the data from bitbucket and generates the html.
 * Paramaters:
 *       $title - This is the title for the profile window
 *       $accountName - This is the bit bucket account name.
 *       $email - This is the email address you want displayed
 *                in the form. Requesting the email from bitbucket
 *                would require us to authoerize a user so this is simpler.
 *
 ***********************************************/
 
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP Bitbicket Profile</title>
<style>
html {
height:100%;	
}
body {
	background-color:#222;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	margin:0px;
	padding:0px;
	height:100%;
}

.bitbucketProfile, setup{
	width:450px;
	background-color:#111;
	border:1px solid #555;	
	margin:30px auto;
	font-family: 'Alegreya Sans SC', sans-serif;
	color:#fff;
	font-size:14px;

}


.bitbucketProfile .title{
	margin:10px;
	font-size:18px;
	text-align:center;
	background-color:#0A0A0A;
}

.bitbucketProfile > div
{
	margin-left:20px;
	line-height:30px;	
}

.bitbucketProfile  a{
	color:#F90;
	text-decoration:none;
	font-size:14px;
}

.bitbucketProfile  a:hover{
	color:#F90;
	text-decoration:underline;
}

.bitbucketProfile > .repo{
	margin:20px;
	font-size:12px;	
	display:block;
	line-height:14px;
}

.bitbucketAvatar{
	float:left;
	margin-left:20px;
	margin-top:10px
	;margin-right:10px;
	border-radius:50%;
	border:1px solid #000;
}
</style>
</head>
<body>
<?php generateBitBucketProfile("PHP Account Framework", "630Studios", "StrikeBackStudios@gmail.com");?>
</body>
</html>



<?php
function generateBitBucketProfile($title, $accountName, $email)
{
	$jSonData = json_decode(file_get_contents("https://api.bitbucket.org/1.0/users/" . $accountName));
	?>
    <div class="bitbucketProfile">
    	<div  class="title"><?=$title;?></div>
   	    <img class="bitbucketAvatar" src="https://bitbucket.org/account/<?=$accountName;?>/avatar/64/" />
        <div>Created By: <?=$jSonData->user->first_name;?> <?=$jSonData->user->last_name;?></div>
		<div>Email: <?=$email;?></div>
		<div>Bitbucket: <a href="http://bitbucket.org/<?=$accountName;?>/" target="_blank">http://bitbucket.org/<?=$accountName;?>/</a></div>
    	<div class="title">Repositories</div>
	<?php
	foreach ($jSonData->repositories as $repo)
	{
		?>
		<div class="repo">
	        <a href="http://bitbucket.org/<?=$accountName;?>/<?=$repo->slug;?>/"  target="_blank"><?=$repo->name;?></a><br />
			<?=$repo->description?>
        </div>
        <?php
	}
}
?>
