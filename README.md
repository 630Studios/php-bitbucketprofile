# PHP Bitbucket Profile #
---  

**Description:** Generates a clean mini bitbucketProfile for use in a php website.  
It pulls a First name, Last name, Avatar Image, and Repository List from bitbucket  
based on the supplied account name.

**Created By:** Jason Penick  
**Created On:** 2/20/2016  
**Email:** StrikeBackStudios@gmail.com  
 
**LICENSE:** APDGUT V1 (Anti Profit, Demonology, Gnome and Unicorn Trafficing License)  
You can do what you want with this except the follow:  
1) Claim you wrote it and sell it for one billion dollars  
2) Play it backwards on a record player while chanting naked in the forest.  
3) Trade this source code with a gnome for a unicorn in return.  
  
**FUNCTIONS**  
**Name:** generateBitBucketProfile($title, $accountName, $email)  
**Description:** Pulls the data from bitbucket and generates the html.  
**Paramaters:** 
**$title** - This is the title for the profile window  
**$accountName** - This is the bit bucket account name.  
**$email** - This is the email address you want displayed in the form. Requesting the email from bitbucket would require us to authorize a user so this is simpler.